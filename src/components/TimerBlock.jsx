import React, {useState, useEffect} from "react";

export default function TimerBlock(props) {
    
    let [timer, setTimer] = useState(props.countdown);
    let [timeInterval, setTimeInterval] = useState(props.countdown);
    let [millis, setMillis] = useState(props.milliseconds);
    let [status, setStatus] = useState(false);
    let [toggleBtnName, setToggleBtnName] = useState("Start");

    useEffect(() => {
        setTimer(props.countdown);
        setMillis(props.milliseconds);
    }, [props.countdown, props.milliseconds])

    return (
        <div className="TimerBlock">
            <h2>{props.tab}</h2>
            <h1 className="countdown">{timer}</h1>

            <div className="buttons">
                <button className="button" id="toggle" onClick={() => {
                    if (status === false) {
                        setTimeInterval(setInterval(() => {
                            if (millis <= 1000) {
                                clearInterval(timeInterval);
                            }
                            setMillis(millis-=1000);
                            const hours = Math.floor(millis / 36e5)
                            const mins = Math.floor((millis % 36e5) / 6e4)
                            const secs = Math.floor((millis % 6e4) / 1000)
                            setTimer(`${hours} : ${mins} : ${secs}`);
                            setToggleBtnName("Stop");
                        }, 1000));
                        setStatus(true);
                    } else {
                        clearInterval(timeInterval);
                        setStatus(false);
                        setToggleBtnName("Start");
                    }
                }}>{toggleBtnName}</button>
                <button className="button" id="reset" onClick={() => {
                    setTimer(props.countdown);
                    clearInterval(timeInterval);
                    setMillis(props.milliseconds);
                    setStatus(false);
                    setToggleBtnName("Start");
                }}>Reset</button>
            </div>
        </div>
    );
}