import React, { useState } from "react";

import './App.css';
import TimerBlock from "./components/TimerBlock";

import logo from './assets/logo.svg';

function App() {
  const [tab, setTab] = useState("pomodoro");
  const [countdown, setCountdown] = useState("00 : 25 : 00");
  const [milliseconds, setMilliseconds] = useState(Math.floor(1.5e6));
  const [bgColor, setBgColor] = useState("#E80000");
  const [pomodoroActive, setPomodoroActive] = useState(true);
  const [shortBreakActive, setShortBreakActive] = useState(false);
  const [longBreakActive, setLongBreakActive] = useState(false);
  document.body.style.backgroundColor = bgColor;
  return (
    <div className="App">
      <img src={logo} className="Logo" alt="logo" />
      <div className="tabs">
        <div className="tab"
        onClick={() => {
          setPomodoroActive(true);
          setShortBreakActive(false);
          setLongBreakActive(false);

          setTab("pomodoro");
          setCountdown("00 : 25 : 00");
          setMilliseconds(Math.floor(1.5e6));
          setBgColor("#E80000");
        }}>
          <p id="pomodoro-tab"
          style={{
            backgroundColor: pomodoroActive ? "#00000026" : "#00000000"
          }}>Pomodoro</p>
        </div>
        <div className="tab"
        onClick={() => {
          setPomodoroActive(false);
          setShortBreakActive(true);
          setLongBreakActive(false);

          setTab("short break");
          setCountdown("00 : 05 : 00");
          setMilliseconds(Math.floor(300000));
          setBgColor("#E86200");
        }}>
          <p id="short-break-tab"
          style={{
            backgroundColor: shortBreakActive ? "#00000026" : "#00000000"
          }}>Short Break</p>
        </div>
        <div className="tab"
        onClick={() => {
          setPomodoroActive(false);
          setShortBreakActive(false);
          setLongBreakActive(true);

          setTab("long break");
          setCountdown("00 : 20 : 00");
          setMilliseconds(Math.floor(1.2e6));
          setBgColor("#E8BB00");
        }}>
          <p id="long-break-tab"
          style={{
            backgroundColor: longBreakActive ? "#00000026" : "#00000000"
          }}>Long Break</p>
        </div>
      </div>

      <TimerBlock tab={tab} countdown={countdown} milliseconds={milliseconds} />
    </div>
  );
}

export default App;
